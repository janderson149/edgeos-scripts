# EdgeOS Scripts - Blacklist.py

### Description 
This script creates and updates a firewall address group on the router using the 
[Emerging Threats Blacklist](http://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt)


Script Requirements: wget 
* [How to install packages on router](https://help.ubnt.com/hc/en-us/articles/205202560-EdgeMAX-Add-other-Debian-packages-to-EdgeOS)
* `apt-get install wget`


Script Usage:
* Put script in the `/config/user-data` directory
* Run `chmod 750 blacklist.py` to add executable permissions to the script
* Run `sudo ./blacklist.py -n <networkgroup>` to execute the python script

Creating A Cronjob:
> Thanks to 'waterside' from the community.ubnt.com forums for contributing this section.

Manipulating cron entries directly on EdgeOS is subject to loss during uprades.
It is far preferable to use the native EdgeOS configuration for scheduled jobs, which will be persistent:
```
set system task-scheduler task run_blacklist_py executable path /config/user-data/blacklist.py
set system task-scheduler task run-blacklist_py crontab-spec "0 2 * * *"
```
or
```
set system task-scheduler task run_blacklist_py executable path /config/user-data/blacklist.py
set system task-scheduler task run-blacklist_py interval 24h
```
The former runs the script once/day at 2am, that latter runs the script once/day every 24 hours from router restart.  The times/interval may be adjusted as needed.